use histogram;
use hound;
use pbr;
use std::env;
use std::time::Duration;

const CHUNK: usize = 1024;

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = &args[1];

    let mut reader = hound::WavReader::open(input_file).unwrap();
    let length = reader.duration();

    let bins: i32 = 100;
    let mut histogram = histogram::Histogram::configure()
        .max_value(bins as u64)
        .build()
        .unwrap();

    let mut pb = pbr::ProgressBar::on(std::io::stderr(), length as u64);
    pb.set_max_refresh_rate(Some(Duration::from_millis(100)));

    let mut buffer: [f32; CHUNK] = [0.; CHUNK];
    let mut count = 0;
    for opt_sample in reader.samples::<i32>() {
        let raw_sample = opt_sample.unwrap() as f32 / i32::MAX as f32;

        buffer[count % CHUNK] = raw_sample;
        if count % CHUNK == CHUNK - 1 {
            let chunk_rms = rms(buffer);
            buffer = [0.; CHUNK];

            let sample = to_db(chunk_rms);
            let bin = ((sample as i32) + bins).min(bins - 1).max(0) as u64;
            histogram.increment(bin).unwrap();
        }

        if count % 1000 == 0 {
            pb.add(1000);
        }

        count += 1;
    }

    pb.finish_print("Finished analysis");
    eprintln!();

    let p_99_8 = histogram.percentile(99.8).unwrap() as i32 - bins + 1;
    let p_99 = histogram.percentile(99.).unwrap() as i32 - bins + 1;
    let p_90 = histogram.percentile(90.).unwrap() as i32 - bins + 1;
    let p_60 = histogram.percentile(60.).unwrap() as i32 - bins + 1;

    // 99.8% = the maximum
    // 99%   = 1 dB headroom
    // 90%   = 2 dB headroom
    // 60%   = 5 dB headroom

    let level3_in = p_60;
    let level2_in = p_90;
    let level1_in = p_99;
    let maximum_in = p_99_8;

    // hard limiter
    println!(
        "compand 0.01,0.1 {:.1},{:.1},0,{:.1} 0 -90 0.2",
        maximum_in + 1,
        maximum_in + 1,
        maximum_in + 2
    );

    // softer limiter
    println!("compand 0.9,1 1:{:.1},{:.1},{:.1},{:.1} 0 -90 1",
             maximum_in, maximum_in,
             0.0, maximum_in);

    // level 1
    println!("compand 0.7,1 1:{:.1},{:.1},{:.1},{:.1} 0 -90 0.9",
             level1_in, level1_in,
             0.0, level1_in + 1);

    // level 2
    println!("compand 0.2,1 1:{:.1},{:.1},{:.1},{:.1} 0 -90 0.3",
             level2_in, level2_in,
             0.0, level2_in + 2);

    // level 3
    println!("compand 0.1,1 1:{:.1},{:.1},{:.1},{:.1} 0 -90 0.2",
             level3_in, level3_in,
             0.0, level3_in + 5);


//     println!("compand 0.07,0.3 6:-100,-100,{:.1},{:.1},{:.1},{:.1},{:.1},{:.1},{:.1},{:.1},0,{:.1} 0 -90 0.17",
//              level3_in, level3_out,
//              level2_in, level2_out,
//              level1_in, level1_out,
//              maximum_in, maximum_out,
//              maximum_out);

    // normalise
    // println!("vol {:.1}dB 0.05", 0 - (level3_in + 5 + 5));
    eprintln!("Final recomended peak: {:.1}", maximum_in + 2)
    // println!("gain -n -0.2");
}

fn rms(chunk: [f32; CHUNK]) -> f32 {
    let mut total = 0.;
    for sample in chunk.iter() {
        total += sample * sample;
    }

    let mean = total / (CHUNK as f32);
    let sqrt = mean.sqrt();
    return sqrt;
}

fn to_db(sample: f32) -> f32 {
    return 20. * sample.abs().log10().max(-100.);
}
